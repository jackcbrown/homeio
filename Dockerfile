FROM node:14 AS base

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . ./


FROM base AS production

RUN npm run build